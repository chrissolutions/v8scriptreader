///////////////////////////////////////////////////////////////////////////////
// V8Exception.h
//
// V8 exception managed defintion.
//
// Author: chrwal
// Created: 14 Jul 2010
//
// Revision history:
// 14 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Converter.h"

using namespace System;

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Exception
// 
// Description:
//  V8 exception class.
///////////////////////////////////////////////////////////////////////////
public ref class V8Exception : public Exception
{
protected:
    V8Exception() {};

internal:
    // Constructors.
    V8Exception(V8TryCatch& tryCatch);
    V8Exception(String^ message) : Exception(message) {}

    // Destructor
    virtual ~V8Exception() {};
        
public:
    // Properites.
    virtual property String^ Message { String^ get() override; }
    virtual property String^ Source { String^ get() override; }
    virtual property int LineNumber { int get(); }

private:
	String^ m_message;
	String^ m_source;
	int     m_lineNumber;
};

NAMESPACE_END