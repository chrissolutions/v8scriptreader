///////////////////////////////////////////////////////////////////////////////
// V8Callback.cpp
//
// V8 delegate class implementation file.
//
// Author: chrwal
// Created: 30 Jul 2010
//
// Revision history:
// 30 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Callback.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Callback constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Callback::V8Callback(V8ValueHandle value)
{
    _ASSERTE(value->IsFunction());
    m_callback = V8Function::Cast(*value);
}

////////////////////////////////////////////////////////////////////
// V8Callback destructor.
//
// Description:
//  Dispose V8Type.
////////////////////////////////////////////////////////////////////
V8Callback::~V8Callback()
{
}

////////////////////////////////////////////////////////////////////
// V8Callback::Initialize
//
// Description:
//  Initalize the V8 Delegate.
////////////////////////////////////////////////////////////////////
//void V8Callback::Initialize()
//{
//	V8HandleScope scope;
//
//    // Set up delegate.
//	V8ValueHandle external = v8::External::New(this->ToPointer());
//    m_delegate = new V8CallbackHandle(V8FunctionTemplate::New(Callback, external)->GetFunction());
//}

////////////////////////////////////////////////////////////////////
// Object^ Invoke(array<Object^>^ args)
//
// Description:
//  Invoke the V8 callback
////////////////////////////////////////////////////////////////////
Object^ V8Callback::Invoke(array<Object^>^ args)
{
    V8HandleScope handleScope;
    V8FunctionHandle callbackHandle(m_callback);

    // Obtain number of managed parameters.
    int length = (args != nullptr) ? args->Length : 0;
	V8ValueHandle* v8args = new V8ValueHandle[length];

    // Convert parameters to V8 values.
    for (int ii = 0; ii < length; ++ii)
    {
        v8args[ii] = V8Converter::ToV8Value(args[ii]);
    }

    // Call the V8 function.
    try
    {
		V8ValueHandle result = callbackHandle->Call(callbackHandle, length, v8args);
		return V8Converter::ToManaged(result);
    }
    finally
    {
        delete v8args;
    }

    return nullptr;
}

////////////////////////////////////////////////////////////////////
// operator Callback^ (V8Callback^ callback)
//
// Description:
//  Invoke the V8 callback
////////////////////////////////////////////////////////////////////
V8Callback::operator Delegate^ (V8Callback^ callback)
{
    return gcnew Callback(callback, &V8Callback::Invoke);
}

NAMESPACE_END