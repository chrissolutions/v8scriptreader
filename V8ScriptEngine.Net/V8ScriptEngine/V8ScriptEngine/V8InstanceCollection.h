///////////////////////////////////////////////////////////////////////////////
// V8InstanceCollection.h
//
// V8 instance collection header definition file.
//
// Author: chrwal
// Created: 13 Jul 2010
//
// Revision history:
// 13 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Instance.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8InstanceCollection
// 
// Description:
//  V8 instance collection class.
///////////////////////////////////////////////////////////////////////////
private ref class V8InstanceCollection : public List<V8Instance^>
{
internal:
    V8InstanceCollection() : List<V8Instance^>() {}

private:
    ~V8InstanceCollection() { Clear(); }
};

NAMESPACE_END
