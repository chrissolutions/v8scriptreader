// V8Globals.h

#pragma once

#include "stdafx.h"
#include "V8Handles.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Script
// 
// Description:
//  V8 Script managed class wrapper.
///////////////////////////////////////////////////////////////////////////
class V8Globals
{
public:
    static V8ValueHandle Print(const V8Arguments& args);
}; 

NAMESPACE_END