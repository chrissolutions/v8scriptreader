///////////////////////////////////////////////////////////////////////////////
// V8Script.cpp
//
// Implements V8Script class.
//
// Author: chrwal
// Created: 12 Jul 2010
//
// Revision history:
// 12 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Context.h"
#include "V8Script.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Script() constructor
//
// Description:
//  Constructs an Element wrapper via progID.
//
// Parameters
//  String^ comId   : COM identifier (clsid or progID)
//  IElementHost^   : Associated element host.
////////////////////////////////////////////////////////////////////
V8Script::V8Script() 
: 
    m_source(nullptr),
    m_exception(nullptr),
    m_result(nullptr),
    m_children(nullptr)
{
}

////////////////////////////////////////////////////////////////////
// V8Script() copy constructor
//
// Description:
//  Constructs a copy of the script object.
//
// Parameters
//  V8Script^ script  : Javascript object.
////////////////////////////////////////////////////////////////////
V8Script::V8Script(V8Script^ script)
: 
    m_source(nullptr),
    m_exception(nullptr),
    m_result(nullptr),
    m_children(nullptr)
{
    _ASSERTE(script != nullptr);
    m_source = script->m_source;
    m_children = script->m_children;
}
    
////////////////////////////////////////////////////////////////////
// V8Script::~V8Script()
//
// Description:
//  Destroys the script.
////////////////////////////////////////////////////////////////////
V8Script::~V8Script()
{
}

////////////////////////////////////////////////////////////////////
// V8Script::Execute()
//
// Description:
//  Execute the script.
////////////////////////////////////////////////////////////////////
void V8Script::Load(String^ source)
{
    if (source == nullptr)
    {
        throw gcnew ArgumentNullException("source");
    }

    m_source = source;    
}

////////////////////////////////////////////////////////////////////
// V8Script::Execute()
//
// Description:
//  Execute the script.
////////////////////////////////////////////////////////////////////
void V8Script::Execute()
{
    // Do not run if there is no source.
    _ASSERT(m_source != nullptr);

    // Initialize handle scopes and try/catch.
    V8HandleScope handleScope;
    V8TryCatch tryCatch;

    // Clear previous result.
    if (m_exception != nullptr) delete m_exception;
    if (m_result != nullptr) delete m_result;

    // Compile the source and check for errors.
    V8ScriptHandle script = v8::Script::Compile(V8Converter::ToV8String(m_source));
    if (script.IsEmpty())
    {
        m_exception = gcnew V8Exception(tryCatch);
        return;
    }

    // Run the script.
    V8ValueHandle result = script->Run();
    if (result.IsEmpty())
    {
        m_exception = gcnew V8Exception(tryCatch);
        return;
    }

    // Assign the result.
    m_result = V8Converter::ToManaged(result);
}

NAMESPACE_END