///////////////////////////////////////////////////////////////////////////////
// V8ContextCollection.h
//
// V8 javascript context header definition file.
//
// Author: chrwal
// Created: 13 Jul 2010
//
// Revision history:
// 13 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Context.h"

using namespace System;
using namespace System::Collections::Generic;

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// Forward references.
///////////////////////////////////////////////////////////////////////////
ref class V8Context;

    
private ref class V8ContextCollection : public List<V8Context^>
{
internal:
    V8ContextCollection() : List<V8Context^>() {}

private:
    ~V8ContextCollection() { Clear(); }
};

NAMESPACE_END


