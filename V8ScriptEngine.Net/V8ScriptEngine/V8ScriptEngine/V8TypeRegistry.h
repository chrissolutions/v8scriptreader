///////////////////////////////////////////////////////////////////////////////
// V8TypeRegistry.h
//
// V8 type registry class definition file.
//
// Author: chrwal
// Created: 19 Jul 2010
//
// Revision history:
// 19 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Definition.h"
#include "V8Context.h"
#include "V8FuncDef.h"
#include "V8TypeDef.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// Forward references.
///////////////////////////////////////////////////////////////////////////
ref class V8Context;
generic<typename T> where T : Wrapper ref class V8Definition;
ref class V8FuncDef;
ref class V8TypeDef;

///////////////////////////////////////////////////////////////////////////
// V8TypeRegistry
// 
// Description:
//  V8 type registry class.
///////////////////////////////////////////////////////////////////////////
ref class V8TypeRegistry
{
public:
    V8TypeRegistry();

public:
    // Constructors.
    virtual ~V8TypeRegistry();

public:
    generic<typename T> void Register() { Register(T::typeid); }
    generic<typename T> void Register(V8Context^ context) { Register(context, T::typeid); }
    void Register(Type^ type);
    void Register(V8Context^ context, Type^ type);
    void Register(String^ name, Delegate^ delegate);
    void Register(V8Context^ context, String^ name, Delegate^ delegate);

private:
    bool IsRegistered(V8Context^ context, Type^ type);

private:
    List<V8TypeDef^>    m_typeDefs;
    List<V8FuncDef^>    m_funcDefs;
};

NAMESPACE_END
