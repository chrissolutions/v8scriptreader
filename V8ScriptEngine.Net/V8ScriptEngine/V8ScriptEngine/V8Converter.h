///////////////////////////////////////////////////////////////////////////////
// V8Converter.h
//
// V8 value converter.
//
// Author: chrwal
// Created: 21 Jul 2010
//
// Revision history:
// 21 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Wrapper.h"
#include "V8Instance.h"
#include "V8Caller.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Converter
// 
// Description:
//  V8 handle wrapper.
///////////////////////////////////////////////////////////////////////////
ref class V8Converter sealed
{
public:
    static Object^ ToManaged(V8ValueHandle value);
    static array<Object^>^ ToManagedParams(const V8Arguments& args);
    static String^ ToString(V8ValueHandle value);
    static Object^ ToObject(V8ValueHandle value);
    static Wrapper^ ToWrapper(V8ValueHandle value);
    static Delegate^ ToCallback(V8ValueHandle value);
    static array<Object^>^ ToArray(V8ValueHandle value);
    generic<typename T> static array<T>^ ToArray(V8ValueHandle value);


    static V8ValueHandle ToV8Value(Object^ value);
    static V8StringHandle ToV8String(String^ value);
    static V8ValueHandle ToV8Array(Array^ value);
    static V8ValueHandle* ToV8Params(array<Object^>^ source, V8ValueHandle* dest, uint length);
};

NAMESPACE_END