///////////////////////////////////////////////////////////////////////////////
// V8Engine.cpp
//
// V8Engine implmentation.
//
// Author: chrwal
// Created: 12 Jul 2010
//
// Revision history:
// 12 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Engine.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Engine constructor
//
// Description:
//  Constucts V8Engine object.
////////////////////////////////////////////////////////////////////
V8Engine::V8Engine()
:
    m_rootContext(gcnew V8Context()),
    m_registry(gcnew V8Registry())
{
    _ASSERTE(m_singleton == nullptr);

    V8HandleScope scope;

    // Set current context to root context.
    m_currentContext = m_rootContext;
    
    // Set up the global object for all contexts.
    V8FunctionTemplateHandle functionTemplate = V8FunctionTemplate::New(V8Globals::Print);
    V8FunctionHandle function = functionTemplate->GetFunction();

    // Bind the global 'print' function to the C++ Print callback.
    V8ObjectHandle global = m_currentContext->Global();
    global->Set(V8String::New("print"), function, v8::ReadOnly);
}

////////////////////////////////////////////////////////////////////
// CurrentContext::get()
//
// Description:
//  Gets the V8Engine running context.
////////////////////////////////////////////////////////////////////
V8Context^ V8Engine::CurrentContext::get()
{
    return Instance->m_currentContext;
}

////////////////////////////////////////////////////////////////////
// RootContext::get()
//
// Description:
//  Gets the V8Engine root context.
////////////////////////////////////////////////////////////////////
V8Context^ V8Engine::RootContext::get()
{
    return Instance->m_rootContext;
}

////////////////////////////////////////////////////////////////////
// Result::get()
//
// Description:
//  Gets the V8Engine result.
////////////////////////////////////////////////////////////////////
Object^ V8Engine::Result::get()
{
    return Instance->m_result;
}

////////////////////////////////////////////////////////////////////
// Registry::get()
//
// Description:
//  Gets the V8Engine registry.
////////////////////////////////////////////////////////////////////
V8Registry^ V8Engine::Registry::get()
{
    return Instance->m_registry;
}

////////////////////////////////////////////////////////////////////
// Instance::get()
//
// Description:
//  Gets the V8Engine instance.
////////////////////////////////////////////////////////////////////
V8Engine^ V8Engine::Instance::get()
{
    // Create singleton if not initialized.
    if (m_singleton == nullptr)
    {
        lock mutex(m_syncRoot);
        m_singleton = gcnew V8Engine();
    }

    // Return singleton.
    return (V8Engine^) m_singleton;
}

////////////////////////////////////////////////////////////////////
// V8Engine::Register()
//
// Description:
//  Registers type to the current context.
////////////////////////////////////////////////////////////////////
generic<typename T>
void V8Engine::Register()
{
    Instance->m_registry->Register<T>();
}

////////////////////////////////////////////////////////////////////
// V8Engine::Register()
//
// Description:
//  Registers type to the current context.
////////////////////////////////////////////////////////////////////
generic<typename T>
void V8Engine::Register(String^ name)
{
    Instance->m_registry->Register<T>(name);
}

////////////////////////////////////////////////////////////////////
// V8Engine::Register()
//
// Description:
//  Registers type to the current context.
////////////////////////////////////////////////////////////////////
generic<typename T> 
void V8Engine::Register(String^ name, T instance)
{
    Instance->m_registry->Register<T>(name, instance);
}

////////////////////////////////////////////////////////////////////
// V8Engine::RegisterDelegate(String^ name, Delegate^ delegate)
//
// Description:
//  Registers delegate to the current context.
////////////////////////////////////////////////////////////////////
void V8Engine::RegisterDelegate(String^ name, Delegate^ delegate)
{
    Instance->m_registry->Register(name, delegate);
}

NAMESPACE_END