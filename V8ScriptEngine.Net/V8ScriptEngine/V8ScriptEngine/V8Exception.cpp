///////////////////////////////////////////////////////////////////////////////
// V8Exception.cpp
//
// Implements V8 exception class.
//
// Author: chrwal
// Created: 14 Jul 2010
//
// Revision history:
// 14 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Exception.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Exception constructor
//
// Description:
//  Constucts V8Exception object.
////////////////////////////////////////////////////////////////////
V8Exception::V8Exception(v8::TryCatch& tryCatch) 
:
    System::Exception(),
    m_message(nullptr),
    m_source(nullptr),
    m_lineNumber(0)
{
    V8Exception();

    // Get the exception message.
    m_message = V8Converter::ToString(tryCatch.Exception());

    // Get the exception details.
	V8MessageHandle messageDetails = tryCatch.Message();
    if (!messageDetails.IsEmpty())
    {
        m_source = V8Converter::ToString(messageDetails->GetScriptResourceName());
        m_lineNumber = messageDetails->GetLineNumber();
    }        
}

////////////////////////////////////////////////////////////////////
// Message::get()
//
// Description:
//  Gets the V8Exception message.
////////////////////////////////////////////////////////////////////
String^ V8Exception::Message::get()
{
    return m_message;
}

////////////////////////////////////////////////////////////////////
// Source::get()
//
// Description:
//  Gets the V8Exception source.
////////////////////////////////////////////////////////////////////
String^ V8Exception::Source::get()
{
    return m_source;
}

////////////////////////////////////////////////////////////////////
// LineNumber::get()
//
// Description:
//  Gets the V8Exception line number.
////////////////////////////////////////////////////////////////////
int V8Exception::LineNumber::get()
{
    return m_lineNumber;
}

NAMESPACE_END