///////////////////////////////////////////////////////////////////////////////
// V8Caller.h
//
// V8 javascript callback caller wrapper.
//
// Author: chrwal
// Created: 31 Jul 2010
//
// Revision history:
// 31 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Wrapper.h"
#include "V8Converter.h"
#include "V8Callback.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Caller
// 
// Description:
//  Provides generic type safety of V8 handle wrapper.
///////////////////////////////////////////////////////////////////////////
ref class V8Caller : public Wrapper
{
protected:
    explicit V8Caller() {};

public:
    // Constructors.
    explicit V8Caller(V8ValueHandle value);
    virtual ~V8Caller();

    // Methods.
    Object^ Invoke(array<Object^>^ args);

    // Operators.
    static operator Delegate^ (V8Caller^ callback);

private:
    V8Function* m_callback;
};

NAMESPACE_END