///////////////////////////////////////////////////////////////////////////////
// V8Wrapper.h
//
// V8 javascript wrappers.
//
// Author: chrwal
// Created: 14 Jul 2010
//
// Revision history:
// 14 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Wrapper
// 
// Description:
//  Provides generic type safety of V8 handle wrapper.
///////////////////////////////////////////////////////////////////////////
ref class Wrapper abstract
{
protected:
    // Constructors.
    Wrapper() { }
    Wrapper(Object^ object) : m_object(object)
    {  
        _ASSERTE(m_object != nullptr);
        m_gcHandle = GCHandle::Alloc(this);
        Initialize();
    }

    // Destructor.
    virtual ~Wrapper()
    { 
        m_gcHandle.Free();
    }

public:
    property Object^ Internal { Object^ get() { return m_object; } }

    // Returns wrapped object as native pointer.
    void* ToPointer()
    { 
        return GCHandle::ToIntPtr(m_gcHandle).ToPointer();
    }

protected:
    virtual void Initialize() {}

private:
    GCHandle m_gcHandle;
    Object^ m_object;
};

///////////////////////////////////////////////////////////////////////////
// V8Wrapper
// 
// Description:
//  V8 handle wrapper.
///////////////////////////////////////////////////////////////////////////
generic<typename T> ref class V8Wrapper : public Wrapper
{
protected:

    // Constuctors.
    V8Wrapper() { _ASSERTE(false); }
    V8Wrapper(T object) : Wrapper(object) {}
    virtual ~V8Wrapper() {}

public:
    // Returns the wrapped object.
    property T Internal
    { 
        T get() { return safe_cast<T>(((Wrapper^)this)->Internal); }
    }
};

NAMESPACE_END