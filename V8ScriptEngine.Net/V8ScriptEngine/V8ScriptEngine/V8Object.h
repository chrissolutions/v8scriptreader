///////////////////////////////////////////////////////////////////////////////
// V8Object.h
//
// V8 native object definition.
//
// Author: chrwal
// Created: 05 Aug 2010
//
// Revision history:
// 05 Aug 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Object
// 
// Description:
//  V8 native object class.
///////////////////////////////////////////////////////////////////////////
public ref class V8Object
{
protected:
    explicit V8Object() {};

public:
    // Constructors.
    explicit V8Object(V8ObjectHandle handle);
    virtual ~V8Object();

public:
    Object^ GetProperty(String^ propertyName);
    void SetProperty(String^ propertyName, Object^ value) { /* TODO */ }
    property array<String^>^ PropertyNames { array<String^>^ get(); }

private:
    V8NativeHandle::Ptr m_native;
};

NAMESPACE_END