///////////////////////////////////////////////////////////////////////////////
// V8Context.h
//
// V8 javascript context header definition file.
//
// Author: chrwal
// Created: 12 Jul 2010
//
// Revision history:
// 12 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Globals.h"
#include "V8Script.h"
#include "V8Exception.h"

using namespace System;
using namespace System::IO;

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// Forward references.
///////////////////////////////////////////////////////////////////////////
ref class V8Script;
ref class V8Exception;

///////////////////////////////////////////////////////////////////////////
// V8Context
// 
// Description:
//  V8 Context managed class wrapper.
///////////////////////////////////////////////////////////////////////////
public ref class V8Context
{
internal:
    V8Context();

public:
    // Constructors.
    V8Context(V8Context^ parent);
    virtual ~V8Context();

public:
    property Object^ Result                 { Object^ get(); }
    property V8Exception^ Exception         { V8Exception^ get(); }
    property V8Script^ RootScript           { V8Script^ get() { return m_rootScript; } }

    void Run();
    void Run(String^ source);

internal:
    V8ObjectHandle Global();
    
private:
    V8Script^               m_rootScript;
    V8ContextHandle::Ptr    m_context;
};

NAMESPACE_END
