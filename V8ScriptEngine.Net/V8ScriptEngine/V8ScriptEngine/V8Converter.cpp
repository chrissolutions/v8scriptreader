///////////////////////////////////////////////////////////////////////////////
// V8Converter.cpp
//
// V8 value converter implmentation file.
//
// Author: chrwal
// Created: 14 Jul 2010
//
// Revision history:
// 14 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Converter.h"
#include "V8Object.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////////
// ToV8Value
//
// Description:
//  Converts managed object to a V8 value.
///////////////////////////////////////////////////////////////////////////////
V8ValueHandle V8Converter::ToV8Value(Object^ value)
{
    if (value != nullptr)
    {
        TypeCode typeCode = Type::GetTypeCode(value->GetType());
        switch (typeCode)
        {
            case TypeCode::Boolean:
                return ((bool)value) ? V8True() : V8False();

            case TypeCode::DateTime:
            {
		        DateTime origin = DateTime(1970, 1, 1);
                DateTime current = (DateTime)value;
		        TimeSpan ts = TimeSpan::FromTicks(current.Ticks - origin.Ticks);
		        return v8::Date::New(ts.TotalMilliseconds);
            }

            case TypeCode::SByte:
            case TypeCode::Int16:
            case TypeCode::Int32:
                return V8Int32::New((int)value);

            case TypeCode::Byte:
            case TypeCode::UInt16:
            case TypeCode::UInt32:
                return V8UInt32::New((uint)value);

            case TypeCode::Int64:
            case TypeCode::UInt64:
            case TypeCode::Single:
            case TypeCode::Double:
                return v8::Number::New((double)value);

            case TypeCode::String:
                return ToV8String((String^)value);

            case TypeCode::Object:
                return V8External::New((gcnew V8Instance(value))->ToPointer());

            default:
                break;
        }
    }

    return v8::Null();
}

///////////////////////////////////////////////////////////////////////////////
// ToV8String
//
// Description:
//  Converts managed string to V8 string.
///////////////////////////////////////////////////////////////////////////////
V8StringHandle V8Converter::ToV8String(String^ value)
{ 
	pin_ptr<const wchar_t> ptr = PtrToStringChars(value);
	return V8String::New((const uint16_t*) ptr);
}

///////////////////////////////////////////////////////////////////////////////
// ToV8Array
//
// Description:
//  Converts managed array to V8 Array.
///////////////////////////////////////////////////////////////////////////////
V8ValueHandle V8Converter::ToV8Array(Array^ value)
{
    V8ArrayHandle result = V8Array::New();
    int length = value->Length;

    for (int ii = 0; ii < length; ++ii)
    {
        V8ValueHandle key = V8Int32::New(ii);
        result->Set(key, ToV8Value(value->GetValue(ii)));
    }

    return result;
}

///////////////////////////////////////////////////////////////////////////////
// ToV8Param
//
// Description:
//  Converts object array to V8 parameters.
///////////////////////////////////////////////////////////////////////////////
V8ValueHandle* V8Converter::ToV8Params(array<Object^>^ source, V8ValueHandle* dest, uint length)
{
    _ASSERTE(dest != nullptr);

    for (uint ii = 0; ii < length; ++ii)
    {
        dest[ii] = V8Converter::ToV8Value(source[ii]);
    }

    return dest;
}

///////////////////////////////////////////////////////////////////////////////
// ToString
//
// Description:
//  Converts V8 string to managed string.
///////////////////////////////////////////////////////////////////////////////
String^ V8Converter::ToString(V8ValueHandle value)
{
    V8String::AsciiValue ascii(value);
    return gcnew String(*ascii);
}

///////////////////////////////////////////////////////////////////////////////
// ToCallback
//
// Description:
//  Converts V8 string to managed string.
///////////////////////////////////////////////////////////////////////////////
Delegate^ V8Converter::ToCallback(V8ValueHandle value)
{
    return gcnew V8Caller(value);
}

///////////////////////////////////////////////////////////////////////////////
// ToManaged
//
// Description:
//  Converts managed object to V8Value.
///////////////////////////////////////////////////////////////////////////////
Object^ V8Converter::ToManaged(V8ValueHandle value)
{ 
	if (value->IsString())
    {
        return ToString(value);
	}
    else if (value->IsInt32())
    {
        return value->Int32Value();
    }
    else if (value->IsBoolean())
    {
        return value->BooleanValue();
    }
    else if (value->IsArray())
    {
        return ToArray(value);
    }
    else if (value->IsFunction())
    {
        return ToCallback(value);
    }
    else if (value->IsObject())
    {
        return ToObject(value);
    }
    else if (value->IsUndefined() || value->IsNull())
    {
	    return nullptr;
	}

    return nullptr;
}

///////////////////////////////////////////////////////////////////////////////
// ToArray
//
// Description:
//  Converts V8 array to managed array.
///////////////////////////////////////////////////////////////////////////////
generic<typename T> array<T>^ V8Converter::ToArray(V8ValueHandle value)
{ 
	V8Array* v8array = V8Array::Cast(*value);
    uint length = v8array->Length();
    array<T>^ managedArray = gcnew array<T>(length);
    for (uint ii = 0; ii < length; ++ii)
    {
		V8ValueHandle key = V8UInt32::New(ii);
		managedArray[ii] = safe_cast<T>(ToManaged(v8array->Get(key)));
	}

    return managedArray;
}

///////////////////////////////////////////////////////////////////////////////
// ToArray
//
// Description:
//  Converts V8 array to managed array.
///////////////////////////////////////////////////////////////////////////////
array<Object^>^ V8Converter::ToArray(V8ValueHandle value)
{
    return ToArray<Object^>(value);
}

///////////////////////////////////////////////////////////////////////////////
// ToManagedParams
//
// Description:
//  Converts array of V8 arguments to a managed array of object parameters.
///////////////////////////////////////////////////////////////////////////////
array<Object^>^ V8Converter::ToManagedParams(const V8Arguments& args)
{ 
    uint length = args.Length();
    array<Object^>^ params = gcnew array<Object^>(length);
    for (uint ii = 0; ii < length; ++ii)
    {
        params[ii] = ToManaged(args[ii]);
    }

    return params;
}

///////////////////////////////////////////////////////////////////////////////
// V8Converter::ToObject(V8ValueHandle value)
//
// Description:
//  Converts V8 value to managed object.
///////////////////////////////////////////////////////////////////////////////
Object^ V8Converter::ToObject(V8ValueHandle value)
{
    // Obtain v8 object.
    V8ObjectHandle objectHandle = value->ToObject();

    // Convert to managed object.
    return (objectHandle->GetInternalField(0)->IsExternal())
        ? ToWrapper(objectHandle->GetInternalField(0))->Internal
        : gcnew V8Object(objectHandle);
}

///////////////////////////////////////////////////////////////////////////////
// V8Converter::ToWrapper(V8ValueHandle value)
//
// Description:
//  Converts V8 value to managed wrapper.
///////////////////////////////////////////////////////////////////////////////
Wrapper^ V8Converter::ToWrapper(V8ValueHandle value)
{
   	V8External* external = V8External::Cast(*value);
    IntPtr v8Ptr = IntPtr(external->Value());
    GCHandle gch = GCHandle::FromIntPtr(v8Ptr);
    return safe_cast<Wrapper^>(gch.Target);
}

NAMESPACE_END