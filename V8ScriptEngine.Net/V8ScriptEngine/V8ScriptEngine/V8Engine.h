///////////////////////////////////////////////////////////////////////////////
// V8Engine.h
//
// V8 javascript engine header definition file.
//
// Author: chrwal
// Created: 12 Jul 2010
//
// Revision history:
// 12 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Context.h"
#include "V8Registry.h"
#include "V8Globals.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Engine
// 
// Description:
//  V8 Application class.
//
//  This is a singleton class that creates a root context to run 
//  V8 applications.
///////////////////////////////////////////////////////////////////////////
public ref class V8Engine
{
private:
    // Constructors.
    explicit V8Engine();

public:
    // Destructor.
    virtual ~V8Engine() {};

    // RootContext accessor.
    property static V8Context^  RootContext     { V8Context^ get(); }
    property static V8Context^  CurrentContext  { V8Context^ get(); }
    property static Object^     Result          { Object^ get(); }

    // Register to the current context.
    static void RegisterDelegate(String^ name, Delegate^ delegate);
    generic<typename T> static void Register();
    generic<typename T> static void Register(String^ name);
    generic<typename T> static void Register(String^ name, T instance);

    void Run() {};
    void NewContext() {}

internal:
    property static V8Registry^ Registry { V8Registry^ get(); }

private:

    property static V8Engine^ Instance
    {
        V8Engine^ get();
    }

private:
    static volatile V8Engine^      m_singleton;
    static initonly Object^             m_syncRoot = gcnew Object();
    initonly V8Context^                 m_rootContext;
    initonly V8Registry^                m_registry;
    V8Context^                          m_currentContext;
    Object^                             m_result;
    V8Exception^                        m_exception;
};

NAMESPACE_END