///////////////////////////////////////////////////////////////////////////////
// V8Script.h
//
// V8 javascript script definition file.
//
// Author: chrwal
// Created: 18 Jul 2010
//
// Revision history:
// 18 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Converter.h"
#include "V8Context.h"
#include "V8Exception.h"
#include "V8ScriptCollection.h"

using namespace System;
using namespace System::IO;

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// Forward references.
///////////////////////////////////////////////////////////////////////////
ref class V8ScriptCollection;

///////////////////////////////////////////////////////////////////////////
// V8Script
// 
// Description:
//  V8 Script managed class wrapper.
///////////////////////////////////////////////////////////////////////////
public ref class V8Script
{
internal:
    V8Script();
    V8Script(V8Script^ script);

    // Constructors.
    virtual ~V8Script();
        
public:
    // Properties.
    property String^ Source             { String^ get() { return m_source; } }
    property Object^ Result             { Object^ get() { return m_result; } }
    property V8Exception^ Exception     { V8Exception^ get() { return m_exception; } }

    // Methods.
    void Load(Stream^ stream)   {};
    void Load(String^ source);
    void LoadFile(String^ sourceFile) {};
    void Execute();

private:
    void Compile() {};

private:
    String^                 m_source;
    V8Exception^            m_exception;
    Object^                 m_result;
    V8ScriptCollection^     m_children;
};

NAMESPACE_END