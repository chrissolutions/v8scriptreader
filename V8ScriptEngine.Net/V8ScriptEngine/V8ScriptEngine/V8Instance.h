///////////////////////////////////////////////////////////////////////////////
// V8Instance.h
//
// V8 instance class definition file.
//
// Author: chrwal
// Created: 20 Jul 2010
//
// Revision history:
// 20 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Wrapper.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Instance
// 
// Description:
//  V8 instance class.
///////////////////////////////////////////////////////////////////////////
ref class V8Instance : public V8Wrapper<Object^>
{
protected:
    explicit V8Instance() {};

public:
    // Constructors.
    explicit V8Instance(Object^ object) : V8Wrapper(object) {}
    virtual ~V8Instance() {};
};

NAMESPACE_END