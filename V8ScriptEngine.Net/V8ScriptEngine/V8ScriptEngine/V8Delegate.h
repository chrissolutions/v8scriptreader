///////////////////////////////////////////////////////////////////////////////
// V8Delegate.h
//
// V8 delegate class definition file.
//
// Author: chrwal
// Created: 30 Jul 2010
//
// Revision history:
// 30 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Converter.h"
#include "V8Wrapper.h"
#include "V8Context.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Delegate
// 
// Description:
//  V8 instance class.
///////////////////////////////////////////////////////////////////////////
ref class V8Delegate : public V8Wrapper<Delegate^>
{
protected:
    explicit V8Delegate() {};

public:
    // Constructors.
    explicit V8Delegate(String^ name, Delegate^ delegate, V8Context^ context);
    virtual ~V8Delegate();

    property String^ Name { String^ get() { return m_name; } }
    property V8Context^ Context { V8Context^ get() { return m_context; } }

protected:
    virtual void Initialize() override;

private:
    String^ m_name;
    V8Context^ m_context;
};

NAMESPACE_END