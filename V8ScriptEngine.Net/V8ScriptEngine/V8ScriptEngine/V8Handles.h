///////////////////////////////////////////////////////////////////////////////
// V8Handles.h
//
// V8 javascript wrappers.
//
// Author: chrwal
// Created: 14 Jul 2010
//
// Revision history:
// 14 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

// Empty internal Object class to eliminate linker warning
namespace v8 { namespace internal { class Object {}; } }

// Handle definitions.
typedef v8::Handle<v8::Array>                   V8ArrayHandle;
typedef v8::Handle<v8::Function>                V8FunctionHandle;
typedef v8::Handle<v8::Object>                  V8ObjectHandle;
typedef v8::Handle<v8::String>                  V8StringHandle;
typedef v8::Handle<v8::Message>                 V8MessageHandle;
typedef v8::Handle<v8::Script>                  V8ScriptHandle;
typedef v8::Handle<v8::Value>                   V8ValueHandle;
typedef v8::Handle<v8::Template>                V8TemplateHandle;
typedef v8::Handle<v8::FunctionTemplate>        V8FunctionTemplateHandle;
typedef v8::Handle<v8::ObjectTemplate>          V8ObjectTemplateHandle;
typedef v8::Local<v8::String>                   V8LocalString;
typedef v8::Local<v8::Value>                    V8LocalValue;

typedef v8::Persistent<v8::Function>            V8PersistentFunction;
typedef v8::Persistent<v8::Object>              V8PersistentObject;
typedef v8::Persistent<v8::FunctionTemplate>    V8ClassTemplate;

typedef v8::Arguments                           V8Arguments;
typedef v8::AccessorInfo                        V8AccessorInfo;
typedef v8::External                            V8External;
typedef v8::Template                            V8Template;
typedef v8::FunctionTemplate                    V8FunctionTemplate;
typedef v8::ObjectTemplate                      V8ObjectTemplate;
typedef v8::HandleScope                         V8HandleScope;
typedef v8::InvocationCallback                  V8InvocationCallback;
typedef v8::Array                               V8Array;
typedef v8::Function                            V8Function;
typedef v8::Object                              V8Object;
typedef v8::Script                              V8Script;
typedef v8::String                              V8String;
typedef v8::TryCatch                            V8TryCatch;
typedef v8::Value                               V8Value;

typedef v8::Integer                             V8Integer;
typedef v8::Int32                               V8Int32;
typedef v8::Uint32                              V8UInt32;

#define V8True                                  v8::True
#define V8False                                 v8::False
#define V8Undefined                             v8::Undefined

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Handle
// 
// Description:
//  V8 handle wrapper.
///////////////////////////////////////////////////////////////////////////
template <typename T>
class V8Handle
{
public:
    V8Handle() {}
    V8Handle(T handle) : m_handle(handle) {}
    virtual ~V8Handle()  {}
    T Handle() { return m_handle; }

private:
    T  m_handle;
};

///////////////////////////////////////////////////////////////////////////
// V8PersistentHandle
// 
// Description:
//  Persistent handle class.
///////////////////////////////////////////////////////////////////////////
template <typename T>
class V8PersistentHandle : public V8Handle<v8::Persistent<T>>
{
public:
    V8PersistentHandle(v8::Persistent<T> handle) : V8Handle(handle) {}
    virtual ~V8PersistentHandle()  { Handle().Dispose(); }
};

///////////////////////////////////////////////////////////////////////////
// V8ContextHandle
// 
// Description:
//  Context handle class.
///////////////////////////////////////////////////////////////////////////
class V8ContextHandle : public V8PersistentHandle<v8::Context>
{
public:
    typedef V8ContextHandle* Ptr;
    V8ContextHandle() : V8PersistentHandle(v8::Context::New()) { Handle()->Enter(); }
    virtual ~V8ContextHandle()  { Handle()->Exit(); }
}; 

///////////////////////////////////////////////////////////////////////////
// V8ClassTemplateHandle
// 
// Description:
//  Template handle class.
///////////////////////////////////////////////////////////////////////////
class V8ClassTemplateHandle : public V8Handle<V8ClassTemplate>
{
public:
    typedef V8ClassTemplateHandle* Ptr;
    V8ClassTemplateHandle(V8FunctionTemplateHandle handle)
        : V8Handle(V8ClassTemplate::New(handle)) {}
}; 

///////////////////////////////////////////////////////////////////////////
// V8NativeHandle
// 
// Description:
//  Template handle class.
///////////////////////////////////////////////////////////////////////////
class V8NativeHandle : public V8Handle<V8PersistentObject>
{
public:
    typedef V8NativeHandle* Ptr;
    V8NativeHandle(V8ObjectHandle handle)
        : V8Handle(V8PersistentObject::New(handle)) {}
}; 

///////////////////////////////////////////////////////////////////////////
// V8CallbackHandle
// 
// Description:
//  Template handle class.
///////////////////////////////////////////////////////////////////////////
class V8CallbackHandle : public V8Handle<V8PersistentFunction>
{
public:
    typedef V8CallbackHandle* Ptr;
    V8CallbackHandle(V8FunctionHandle handle)
        : V8Handle(V8PersistentFunction::New(handle)) {}
}; 

NAMESPACE_END