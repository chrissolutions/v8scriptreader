///////////////////////////////////////////////////////////////////////////////
// V8Delegate.cpp
//
// V8 delegate class implementation file.
//
// Author: chrwal
// Created: 30 Jul 2010
//
// Revision history:
// 30 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Delegate.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// Callback
//
// Description:
//  V8 Delegate callback.
////////////////////////////////////////////////////////////////////
static V8ValueHandle DelegateCallback(const V8Arguments& args)
{
    V8HandleScope handleScope;

    // Convert into V8Delegate.
    V8Delegate^ v8Delegate = dynamic_cast<V8Delegate^>(V8Converter::ToWrapper(args.Data()));

    // Invoke the delegate.
    // TODO:  Guard against exception.
    Object^ value = v8Delegate->Internal->DynamicInvoke(V8Converter::ToManagedParams(args));

    // Return the value.
    return handleScope.Close(V8Converter::ToV8Value(value));
}

////////////////////////////////////////////////////////////////////
// V8Delegate constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Delegate::V8Delegate(String^ name, Delegate^ delegate, V8Context^ context) 
:
    V8Wrapper(delegate),
    m_name(name),
    m_context(context)
{
    _ASSERTE(name != nullptr);
    _ASSERTE(context != nullptr);
}

////////////////////////////////////////////////////////////////////
// V8Delegate destructor.
//
// Description:
//  Dispose V8Type.
////////////////////////////////////////////////////////////////////
V8Delegate::~V8Delegate()
{
}

////////////////////////////////////////////////////////////////////
// V8Delegate::Initialize
//
// Description:
//  Initalize the V8 Delegate.
////////////////////////////////////////////////////////////////////
void V8Delegate::Initialize()
{
	V8HandleScope scope;

    // Set up delegate.
	V8ValueHandle external = v8::External::New(ToPointer());
	V8FunctionTemplateHandle functionTemplate = V8FunctionTemplate::New(DelegateCallback, external);
    V8ObjectHandle global = Context->Global();
    global->Set(V8Converter::ToV8String(Name), functionTemplate->GetFunction());
}

NAMESPACE_END