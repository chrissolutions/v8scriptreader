///////////////////////////////////////////////////////////////////////////////
// V8Caller.cpp
//
// V8 delegate class implementation file.
//
// Author: chrwal
// Created: 30 Jul 2010
//
// Revision history:
// 30 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Caller.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Caller constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Caller::V8Caller(V8ValueHandle value)
{
    _ASSERTE(value->IsFunction());
    m_callback = V8Function::Cast(*value);
}

////////////////////////////////////////////////////////////////////
// V8Caller destructor.
//
// Description:
//  Dispose V8Type.
////////////////////////////////////////////////////////////////////
V8Caller::~V8Caller()
{
}

////////////////////////////////////////////////////////////////////
// V8Caller::Initialize
//
// Description:
//  Initalize the V8 Delegate.
////////////////////////////////////////////////////////////////////
//void V8Caller::Initialize()
//{
//	V8HandleScope scope;
//
//    // Set up delegate.
//	V8ValueHandle external = v8::External::New(this->ToPointer());
//    m_delegate = new V8CallerHandle(V8FunctionTemplate::New(Callback, external)->GetFunction());
//}

////////////////////////////////////////////////////////////////////
// Object^ Invoke(array<Object^>^ args)
//
// Description:
//  Invoke the V8 callback
////////////////////////////////////////////////////////////////////
Object^ V8Caller::Invoke(array<Object^>^ args)
{
    V8HandleScope scope;
    V8FunctionHandle callbackHandle(m_callback);
    Object^ retval = nullptr;

    // Obtain number of managed parameters.
    int length = (args != nullptr) ? args->Length : 0;

    // Convert managed parameters to V8 values.
    V8ValueHandle* v8args = V8Converter::ToV8Params(args, new V8ValueHandle[length], length);

    // Call the V8 function.
    try
    {
		V8ValueHandle result = callbackHandle->Call(callbackHandle, length, v8args);
		retval = V8Converter::ToManaged(result);
    }
    finally
    {
        delete v8args;
    }

    return retval;
}

////////////////////////////////////////////////////////////////////
// operator Callback^ (V8Caller^ caller)
//
// Description:
//  Invoke the V8 callback
////////////////////////////////////////////////////////////////////
V8Caller::operator Delegate^ (V8Caller^ caller)
{
    return gcnew V8Callback(caller, &V8Caller::Invoke);
}

NAMESPACE_END