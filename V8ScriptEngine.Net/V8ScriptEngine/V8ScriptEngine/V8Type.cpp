///////////////////////////////////////////////////////////////////////////////
// V8Type.cpp
//
// V8 type class implementation file.
//
// Author: chrwal
// Created: 18 Jul 2010
//
// Revision history:
// 18 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Type.h"
#include "V8Method.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// Constructor
//
// Description:
//  Managed type constructor function.
////////////////////////////////////////////////////////////////////
static V8ValueHandle Constructor(const V8Arguments& args)
{
    V8HandleScope handle_scope;

    // Convert into V8Type.
    V8Type^ v8Type = dynamic_cast<V8Type^>(V8Converter::ToWrapper(args.Data()));

    // Instantiate the managed object.
    V8Instance^ instance = (v8Type->IsSingleton)
        ? gcnew V8Instance(v8Type->Instance)
        : v8Type->CreateInstance(V8Converter::ToManagedParams(args));

    // Set the V8 internal to the instance.
    args.This()->SetInternalField(0, V8External::New(instance->ToPointer()));

    // Return this pointer.
    return args.This();
}

////////////////////////////////////////////////////////////////////
// Getter
//
// Description:
//  Generic getter interoperability function.
////////////////////////////////////////////////////////////////////
static V8ValueHandle Getter(V8LocalString accessorName, const V8AccessorInfo &args) 
{ 
    V8HandleScope handleScope;
  	V8ValueHandle retval = V8Undefined();

    // Get managed type.
    V8Type^ v8Type = dynamic_cast<V8Type^>(V8Converter::ToWrapper(args.Data()));
    Type^ type = v8Type->Internal;

    // Get the property info.
    String^ propertyName = V8Converter::ToString(accessorName);
	PropertyInfo^ info = type->GetProperty(propertyName);

    // Get the property.
	if (info != nullptr && info->CanRead)
    {
        V8Instance^ instance = dynamic_cast<V8Instance^>(V8Converter::ToWrapper(args.This()->GetInternalField(0)));
        _ASSERTE(instance != nullptr);
		Object^ value = info->GetValue(instance->Internal, nullptr);
        retval = handleScope.Close(V8Converter::ToV8Value(value));
	}

    return retval;
}

////////////////////////////////////////////////////////////////////
// Setter
//
// Description:
//  Generic setter interoperability function.
////////////////////////////////////////////////////////////////////
static void Setter(V8LocalString accessorName, V8LocalValue value, const V8AccessorInfo &args) 
{ 
    V8HandleScope handle_scope;

    // Get managed type.
    V8Type^ v8Type = dynamic_cast<V8Type^>(V8Converter::ToWrapper(args.Data()));
    Type^ type = v8Type->Internal;

    // Get the property info.
    String^ propertyName = V8Converter::ToString(accessorName);
	PropertyInfo^ info = type->GetProperty(propertyName);

    // Set the property.
	if (info != nullptr && info->CanRead)
    {
        V8Instance^ instance = dynamic_cast<V8Instance^>(V8Converter::ToWrapper(args.This()->GetInternalField(0)));
        _ASSERTE(instance != nullptr);
		info->SetValue(instance->Internal, V8Converter::ToManaged(value), nullptr);
	}
}

////////////////////////////////////////////////////////////////////
// Method
//
// Description:
//  Generic method interoperability function.
////////////////////////////////////////////////////////////////////
static V8ValueHandle Method(const V8Arguments& args) 
{ 
    V8HandleScope handleScope;

    // Get managed type.
    V8Method^ v8Method = dynamic_cast<V8Method^>(V8Converter::ToWrapper(args.Data()));
    MethodInfo^ method = v8Method->Internal;

    // Invoke the method.
    V8Instance^ instance = dynamic_cast<V8Instance^>(V8Converter::ToWrapper(args.This()->GetInternalField(0)));
    Object^ value = method->Invoke(instance->Internal, V8Converter::ToManagedParams(args));

    // Return the value.
    return handleScope.Close(V8Converter::ToV8Value(value));
}

////////////////////////////////////////////////////////////////////
// Delegate
//
// Description:
//  Invokes managed delegate.
////////////////////////////////////////////////////////////////////
static V8ValueHandle Delegate(const V8Arguments& args)
{
    V8HandleScope handle_scope;

    // Convert into V8Type.
    V8Type^ v8Type = dynamic_cast<V8Type^>(V8Converter::ToWrapper(args.Data()));

    // Instantiate the managed object.
    V8Instance^ instance = v8Type->CreateInstance(V8Converter::ToManagedParams(args));

    // Attach instance to the this object.
    args.This()->SetInternalField(0, V8External::New(instance->ToPointer()));

    // Return this pointer.
    return args.This();
}

////////////////////////////////////////////////////////////////////
// V8Type constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Type::V8Type(String^ name, Type^ type, V8Context^ context)
: 
    V8Wrapper(type),
    m_name(name),
    m_instance(nullptr),
    m_context(context)
{
    _ASSERTE(name != nullptr);
    _ASSERTE(context != nullptr);
}

////////////////////////////////////////////////////////////////////
// V8Type constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Type::V8Type(String^ name, Object^ instance, V8Context^ context)
: 
    V8Wrapper(instance->GetType()),
    m_name(name),
    m_instance(instance),
    m_context(context)
{
    _ASSERTE(name != nullptr);
    _ASSERTE(context != nullptr);
}

////////////////////////////////////////////////////////////////////
// V8Type destructor.
//
// Description:
//  Dispose V8Type.
////////////////////////////////////////////////////////////////////
V8Type::~V8Type()
{
    SAFE_DELETE(m_classTemplate);
}

////////////////////////////////////////////////////////////////////
// V8Type::Initialize
//
// Description:
//  Managed type constructor function.
////////////////////////////////////////////////////////////////////
void V8Type::Initialize()
{
	V8HandleScope scope;

    // Set up external object, class template and context.
	V8ValueHandle external = V8External::New(this->ToPointer());

    // Create the V8 class, instance, and prototype templates.
    m_classTemplate = new V8ClassTemplateHandle(V8FunctionTemplate::New(Constructor, external));
    V8ObjectTemplateHandle instanceTemplate = m_classTemplate->Handle()->InstanceTemplate();
    V8ObjectTemplateHandle prototypeTemplate = m_classTemplate->Handle()->PrototypeTemplate();
    instanceTemplate->SetInternalFieldCount(1);

    // Obtain internal context object.
    V8ObjectHandle global = Context->Global();
        
	// Set up the prototype properties.
 	for each (PropertyInfo^ propertyInfo in Internal->GetProperties())
    {
        V8StringHandle propertyName = V8Converter::ToV8String(propertyInfo->Name);
		prototypeTemplate->SetAccessor(propertyName, Getter, Setter, external);
	}

    // Set up the prototype methods.
	for each (MethodInfo^ methodInfo in Internal->GetMethods())
    {
        V8Method^ method = gcnew V8Method(methodInfo);
		V8FunctionTemplateHandle functionTemplate = V8FunctionTemplate::New(Method, V8External::New(method->ToPointer()));
        prototypeTemplate->Set(V8Converter::ToV8String(methodInfo->Name), functionTemplate->GetFunction());
    }

    if (!IsSingleton)
    {
        // Set up createable object.
        global->Set(V8Converter::ToV8String(Name), m_classTemplate->Handle()->GetFunction());
    }
    else
    {
        // Set up singleton object.
        V8ObjectHandle singleton = instanceTemplate->NewInstance();
        global->Set(V8Converter::ToV8String(Name), singleton);
    }
}

////////////////////////////////////////////////////////////////////
// CreateInstance(array<Object^>^ params)
//
// Description:
//  Create the managed object instance.
////////////////////////////////////////////////////////////////////
V8Instance^ V8Type::CreateInstance(array<Object^>^ params)
{
    return gcnew V8Instance(Activator::CreateInstance(Internal, params));
}

NAMESPACE_END