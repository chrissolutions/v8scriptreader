///////////////////////////////////////////////////////////////////////////////
// V8Registry.h
//
// V8 type registry class definition file.
//
// Author: chrwal
// Created: 19 Jul 2010
//
// Revision history:
// 19 Jul 2010  chrwal      Initial version.
// 30 Jul 2010  chrwal      Renamed V8Registry to V8Registry.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Context.h"
#include "V8Type.h"
#include "V8Delegate.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// Forward references.
///////////////////////////////////////////////////////////////////////////
ref class V8Context;
generic<typename T> where T : Wrapper ref class V8Definition;
ref class V8FuncDef;
ref class V8TypeDef;

///////////////////////////////////////////////////////////////////////////
// V8Registry
// 
// Description:
//  V8 type registry class.
///////////////////////////////////////////////////////////////////////////
ref class V8Registry
{
public:
    V8Registry();

public:
    // Constructors.
    virtual ~V8Registry();

public:
    generic<typename T> void Register();
    generic<typename T> void Register(V8Context^ context);
    generic<typename T> void Register(String^ name);

    generic<typename T> void Register(String^ name, T instance);
    generic<typename T> void Register(V8Context^ context, String^ name, T instance);

    void Register(Type^ type);
    void Register(V8Context^ context, Type^ type);
    void Register(V8Context^ context, String^ name, Type^ type);

    void Register(String^ name, Delegate^ delegate);
    void Register(V8Context^ context, String^ name, Delegate^ delegate);

private:
    bool IsTypeRegistered(V8Context^ context, String^ typeName);
    bool IsDelegateRegistered(V8Context^ context, String^ delegateName);

private:
    List<V8Type^>^      m_types;
    List<V8Delegate^>^  m_delegates;
};

NAMESPACE_END
