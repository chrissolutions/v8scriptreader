///////////////////////////////////////////////////////////////////////////////
// V8Type.h
//
// V8 type class definition file.
//
// Author: chrwal
// Created: 18 Jul 2010
//
// Revision history:
// 18 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Wrapper.h"
#include "V8Converter.h"
#include "V8Instance.h"
#include "V8Delegate.h"
#include "V8InstanceCollection.h"
#include "V8Context.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// Forward references.
///////////////////////////////////////////////////////////////////////////
ref class V8Context;
ref class V8Instance;
ref class V8InstanceCollection;
ref class V8Type;

///////////////////////////////////////////////////////////////////////////
// V8Type
// 
// Description:
//  Manages V8 type.
///////////////////////////////////////////////////////////////////////////
ref class V8Type : public V8Wrapper<Type^>
{
protected:
    explicit V8Type() {};

public:
    // Constructors.
    explicit V8Type(String^ name, Type^ type, V8Context^ context);
    explicit V8Type(String^ name, Object^ instance, V8Context^ context);
    virtual ~V8Type();

    property bool IsSingleton { bool get() { return Instance != nullptr; } }
    property String^ Name { String^ get() { return m_name; } }
    property Object^ Instance { Object^ get() { return m_instance; } }
    property V8Context^ Context { V8Context^ get() { return m_context; } }

    V8Instance^ CreateInstance(array<Object^>^ params);

protected:
    virtual void Initialize() override;

private:
    String^ m_name;
    Object^ m_instance;
    V8Context^ m_context;
    V8ClassTemplateHandle::Ptr m_classTemplate;
};

NAMESPACE_END