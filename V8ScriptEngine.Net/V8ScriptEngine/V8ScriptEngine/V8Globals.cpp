///////////////////////////////////////////////////////////////////////////////
// V8Globals.cpp
//
// Implements global object methods.
//
// Author: chrwal
// Created: 16 Jul 2010
//
// Revision history:
// 16 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Globals.h"

NAMESPACE_BEGIN

// Extracts a C string from a V8 Utf8Value.
const char* ToCString(const V8String::Utf8Value& value) {
  return *value ? *value : "<string conversion failed>";
}

// The callback that is invoked by v8 whenever the JavaScript 'print'
// function is called.  Prints its arguments on stdout separated by
// spaces and ending with a newline.
V8ValueHandle V8Globals::Print(const V8Arguments& args)
{
    bool first = true;
    for (int i = 0; i < args.Length(); i++)
    {
        v8::HandleScope handle_scope;
        if (first)
        {
            first = false;
        }
        else
        {
            printf(" ");
        }

        V8String::Utf8Value str(args[i]);
        const char* cstr = ToCString(str);
        printf("%s", cstr);
    }

    printf("\n");
    fflush(stdout);
    return v8::Undefined();
}

NAMESPACE_END