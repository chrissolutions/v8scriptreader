///////////////////////////////////////////////////////////////////////////////
// V8Registry.cpp
//
// Implements V8 registry.
//
// Author: chrwal
// Created: 19 Jul 2010
//
// Revision history:
// 19 Jul 2010  chrwal      Initial version.
// 30 Jul 2010  chrwal      Renamed V8TypeRegistry to V8Registry.
///////////////////////////////////////////////////////////////////////////////

// This is the main DLL file.

#include "stdafx.h"
#include "V8Engine.h"
#include "V8Registry.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Registry() constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Registry::V8Registry()
:
    m_types(gcnew List<V8Type^>()),
    m_delegates(gcnew List<V8Delegate^>())
{
}

////////////////////////////////////////////////////////////////////
// V8Registry() constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Registry::~V8Registry() 
{
}

////////////////////////////////////////////////////////////////////
// Register() 
//
// Description:
//  Registers managed type using provided name to V8
////////////////////////////////////////////////////////////////////
generic<typename T> void V8Registry::Register()
{
    Register(T::typeid);
}

////////////////////////////////////////////////////////////////////
// Register(V8Context^ context) 
//
// Description:
//  Registers managed type using provided context to V8
////////////////////////////////////////////////////////////////////
generic<typename T> void V8Registry::Register(V8Context^ context)
{
    Register(context, T::typeid);
}

////////////////////////////////////////////////////////////////////
// Register(String^ name) 
//
// Description:
//  Registers managed type using provided name to V8
////////////////////////////////////////////////////////////////////
generic<typename T> void V8Registry::Register(String^ name)
{
    Register(V8Engine::CurrentContext, name, T::typeid);
}

////////////////////////////////////////////////////////////////////
// Register(Type^ type) 
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
void V8Registry::Register(Type^ type) 
{
    Register(V8Engine::CurrentContext, type);
}

////////////////////////////////////////////////////////////////////
// V8Registry() constructor
//
// Description:
//  Registers type to context.
////////////////////////////////////////////////////////////////////
void V8Registry::Register(V8Context^ context, Type^ type) 
{
    Register(context, type->Name, type);
}

////////////////////////////////////////////////////////////////////
// V8Registry() constructor
//
// Description:
//  Registers type to context.
////////////////////////////////////////////////////////////////////
void V8Registry::Register(V8Context^ context, String^ name, Type^ type)
{
    if (context == nullptr)
    {
        throw gcnew ArgumentNullException("context");
    }

    if (String::IsNullOrWhiteSpace(name))
    {
	    throw gcnew ArgumentNullException("name");
    }

    if (type == nullptr)
    {
	    throw gcnew ArgumentNullException("type");
    }

    if (type->IsInterface || type->IsAbstract)
    {
        throw gcnew ApplicationException(type->Name + " is not instantiable.");
    }

    if (!IsTypeRegistered(context, name))
    {
        m_types->Add(gcnew V8Type(name, type, context));
    }
}

////////////////////////////////////////////////////////////////////
// V8Registry() constructor
//
// Description:
//  Registers type to context.
////////////////////////////////////////////////////////////////////
void V8Registry::Register(String^ name, Delegate^ delegate) 
{
    Register(V8Engine::CurrentContext, name, delegate);
}

////////////////////////////////////////////////////////////////////
// Register(V8Context^ context, String^ name, Delegate^ delegate)
//
// Description:
//  Registers type to context.
////////////////////////////////////////////////////////////////////
void V8Registry::Register(V8Context^ context, String^ name, Delegate^ delegate) 
{
    if (context == nullptr)
    {
        throw gcnew ArgumentNullException("context");
    }

    if (name == nullptr)
    {
        throw gcnew ArgumentNullException("name");
    }

    if (delegate == nullptr)
    {
	    throw gcnew ArgumentNullException("delegate");
    }

    if (!IsDelegateRegistered(context, name))
    {
        m_delegates->Add(gcnew V8Delegate(name, delegate, context));
    }
}

////////////////////////////////////////////////////////////////////
// Register(V8Context^ context, String^ name, T instance)
//
// Description:
//  Registers type to context.
////////////////////////////////////////////////////////////////////
generic<typename T>
void V8Registry::Register(String^ name, T instance) 
{ 
    Register(V8Engine::CurrentContext, name, instance);
}

////////////////////////////////////////////////////////////////////
// Register(V8Context^ context, String^ name, T instance)
//
// Description:
//  Registers type to context.
////////////////////////////////////////////////////////////////////
generic<typename T>
void V8Registry::Register(V8Context^ context, String^ name, T instance) 
{
    if (context == nullptr)
    {
        throw gcnew ArgumentNullException("context");
    }

    if (name == nullptr)
    {
        throw gcnew ArgumentNullException("name");
    }

    if (instance == nullptr)
    {
	    throw gcnew ArgumentNullException("instance");
    }

    if (!IsTypeRegistered(context, name))
    {
        m_types->Add(gcnew V8Type(name, instance, context));
    }
}

////////////////////////////////////////////////////////////////////
// bool IsTypeRegistered(V8Context^ context, String^ name)
//
// Description:
//  Constructs the root context.
////////////////////////////////////////////////////////////////////
bool V8Registry::IsTypeRegistered(V8Context^ context, String^ name)
{
    for each(V8Type^ type in m_types)
    {
        // TODO: the search should crawl up the context parent tree 
        // to find the matching context as contexts should inherit
        // the globals from parent contexts.  For now just match context.
        if (type->Context == context && type->Name == name)
            return true;
    }

    return false;
}

////////////////////////////////////////////////////////////////////
// bool IsDelegateRegistered(V8Context^ context, String^ delegateName)
//
// Description:
//  Determines whetherConstructs the root context.
////////////////////////////////////////////////////////////////////
bool V8Registry::IsDelegateRegistered(V8Context^ context, String^ name)
{
    for each(V8Delegate^ delegate in m_delegates)
    {
        // TODO: the search should crawl up the context parent tree 
        // to find the matching context as contexts should inherit
        // the globals from parent contexts.  For now just match context.
        if (delegate->Context == context && delegate->Name == name)
            return true;
    }

    return false;
}

NAMESPACE_END