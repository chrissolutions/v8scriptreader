///////////////////////////////////////////////////////////////////////////////
// V8Object.cpp
//
// V8 native object implementation.
//
// Author: chrwal
// Created: 05 Aug 2010
//
// Revision history:
// 05 Aug 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "V8Converter.h"
#include "V8Object.h"

NAMESPACE_BEGIN

////////////////////////////////////////////////////////////////////
// V8Object constructor
//
// Description:
//  Construcs the root context.
////////////////////////////////////////////////////////////////////
V8Object::V8Object(V8ObjectHandle handle) 
:
    m_native(new V8NativeHandle(handle))
{
}

////////////////////////////////////////////////////////////////////
// V8Object destructor.
//
// Description:
//  Dispose V8Object.
////////////////////////////////////////////////////////////////////
V8Object::~V8Object()
{
    SAFE_DELETE(m_native);
}

////////////////////////////////////////////////////////////////////
// GetProperty(String^ propertyName)
//
// Description:
//  Dispose V8Object.
////////////////////////////////////////////////////////////////////
Object^ V8Object::GetProperty(String^ propertyName)
{
	V8ValueHandle key = V8Converter::ToV8Value(propertyName);
    V8ValueHandle value = m_native->Handle()->Get(key);
    return V8Converter::ToManaged(value);
}

////////////////////////////////////////////////////////////////////
// GetPropertyNames()
//
// Description:
//  Dispose V8Object.
////////////////////////////////////////////////////////////////////
array<String^>^ V8Object::PropertyNames::get()
{
    V8ArrayHandle v8PropertyNames = m_native->Handle()->GetPropertyNames();
    uint length = v8PropertyNames->Length();
    array<String^>^ propertyNames = gcnew array<String^>(length);
    for (uint ii = 0; ii < length; ++ii)
    {
        propertyNames[ii] = V8Converter::ToString(v8PropertyNames->Get(V8UInt32::New(ii)));
    }

    return propertyNames;
}

NAMESPACE_END