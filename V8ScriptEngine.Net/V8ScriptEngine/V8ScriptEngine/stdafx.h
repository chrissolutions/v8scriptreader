// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#include <v8.h>
#include <string>
#include <vector>
#include <vcclr.h>
#include <msclr\lock.h>

using namespace msclr;
using namespace cli;
using namespace std;

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;

// Namespace macros.
#define NAMESPACE_BEGIN namespace V8ScriptEngine {
#define NAMESPACE_END }
#define NAMESPACE V8ScriptEngine

// Safe delete macro.
#define SAFE_DELETE(x)  do { if (x != nullptr) { delete (x); } } while(0)

// Typedefs.
typedef unsigned int uint;
