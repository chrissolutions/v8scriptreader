///////////////////////////////////////////////////////////////////////////////
// V8Method.h
//
// V8 method class definition file.
//
// Author: chrwal
// Created: 24 Jul 2010
//
// Revision history:
// 24 Jul 2010  chrwal      Initial version.
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "V8Handles.h"
#include "V8Wrapper.h"

NAMESPACE_BEGIN

///////////////////////////////////////////////////////////////////////////
// V8Method
// 
// Description:
//  V8 method class definition.
///////////////////////////////////////////////////////////////////////////
ref class V8Method : V8Wrapper<MethodInfo^>
{
protected:
    explicit V8Method() {};

public:
    // Constructors.
    explicit V8Method(MethodInfo^ methodInfo);
    virtual ~V8Method();
};

NAMESPACE_END