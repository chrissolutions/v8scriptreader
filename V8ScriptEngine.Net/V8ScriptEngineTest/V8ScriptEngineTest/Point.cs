﻿using System;
using V8ScriptEngine;

namespace V8ScriptEngineTest
{
    public class Point
    {
        public Point()
        {
        }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }

        public bool IsEmpty()
        {
            return X == 0 && Y == 0;
        }

        public override string ToString()
        {
            return "(X=" + X + ";Y=" + Y + ")";
        }
    }
}
