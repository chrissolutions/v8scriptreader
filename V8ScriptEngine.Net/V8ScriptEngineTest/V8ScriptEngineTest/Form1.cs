﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using V8ScriptEngine;

namespace V8ScriptEngineTest
{
    public partial class Form1 : Form, IApplicationConsole
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1Click(object sender, EventArgs e)
        {
            V8Engine.Register<Point>();
            V8Engine.Register<IApplicationConsole>("Console", this);
            V8Engine.RegisterDelegate("printmsg", new Action<string>(message => Write(message)));
            V8Engine.CurrentContext.Run(richTextBox1.Text);
        }                           

        private void RichTextBox1TextChanged(object sender, EventArgs e)
        {

        }

        private void RichTextBox2TextChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("text changed");
        }

        #region IApplicationConsole Members

        public void Write(string message)
        {
            if (richTextBox2.TextLength > 0)
            {
                richTextBox2.Text += Environment.NewLine;
            }

            richTextBox2.Text += message;
        }

        public void WritePoint(Point point)
        {
            Write(point.ToString());
        }

        public void WriteLambda(V8Callback callback)
        {
            Object result = callback(null);
            string message = result == null ? "Function returned null" : "Function result: " + result.ToString();
            Write(message);
        }

        public void WriteObject(V8Object jsObj)
        {
            foreach (String name in jsObj.PropertyNames)
            {
                Object obj = jsObj.GetProperty(name);
                Write(String.Format("{0} = {1}", name, obj));
            }
        }

        public void Clear()
        {
            richTextBox2.Text = "";
        }

        #endregion
    }
}
