﻿using System;
using V8ScriptEngine;

namespace V8ScriptEngineTest
{
    public interface IApplicationConsole
    {
        void Write(string message);
        void WriteLambda(V8Callback callback);
        void WritePoint(Point point);
        void WriteObject(V8Object jsObj);
        void Clear();
    }
}
